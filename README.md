# LaTeX Thesis Template

This template can be used to write a thesis and does support the following features:
- Custom Title Page
- Custom Header
- Glossary
- Bibliography

